# Data Structures and Algorithms (DSA)

This repository contains Python implementations of various data structures and algorithms. It is a resource for learning about and understanding fundamental concepts in computer science.

## Contents

- Data structures: arrays, linked lists, stacks, queues, trees, graphs
- Algorithms: sorting algorithms, search algorithms, data compression algorithms, encryption algorithms

## Usage

To use these implementations, clone the repository and import the desired data structure or algorithm module in your Python code. 

```python
from data_structures import LinkedList

ll = LinkedList()

from algorithms import bubble_sort

arr = [3, 2, 5, 1, 4]

sorted_arr = bubble_sort(arr)
```
## Contributions

Pull requests are welcome. For major changes, please open an issue first to discuss the changes you would like to make.

## License

This repository is licensed under the GNU General Public License v3.0 - see the [LICENSE](LICENSE) file for details.

